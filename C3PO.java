package UCSAL;
import robocode.*;
import java.awt.Color;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * C3PO - a robot by (your name here)
 */
public class C3PO extends Robot
{
	/**
	 * run: C3PO's default behavior
	 */
	public void run() {
	
		setColors(Color.black,Color.black,Color.black);
		

		// Robot main loop
		while(true) 
		{
		for(int voltasdireita=0;voltasdireita<4;voltasdireita++)
		{
		ahead(100);
		turnRight(90);
		turnGunRight(90);		
		}
		for(int voltasesquerda=4;voltasesquerda>0;voltasesquerda--)
		{
		ahead(100);
		turnLeft(90);
		turnGunLeft(180);
		}
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(3);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	
}
